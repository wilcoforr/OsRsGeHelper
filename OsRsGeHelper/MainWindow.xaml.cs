﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

using Newtonsoft.Json;
using System.Threading.Tasks;


namespace OsRsGeHelper
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constants, Static Properties
        public const string BASE_OS_RS_API_URL = "http://services.runescape.com/";

        public const string ITEM_DETAILS_ROUTE = "m=itemdb_oldschool/api/catalogue/detail.json?item=";

        public const string SAVED_ITEMS_JSON_FILEPATH = @"savedItems.json";

        public const string OSRS_ITEMS_FOR_DROPDOWN = @"osrsitems.json";

        /// <summary>
        /// Simple list for display of items that are in the OS RS GE Database
        /// </summary>
        public static IEnumerable<ItemForDropdown> ItemsForDropdowns;

        /// <summary>
        /// The items added to the datagrid
        /// </summary>
        public static ObservableCollection<Item> ObservableItems = new ObservableCollection<Item>();


        /// <summary>
        /// Used to compare for saving for whether the user should save changes
        /// when they exit
        /// </summary>
        public static int ItemsInitialCount = 0;


        /// <summary>
        /// The HttpClient the app uses to contact the OS RS GE API
        /// Note that this is not in a using statement. If the using statement is used,
        /// lots of sockets are opened on the computer. This makes it so that the app just
        /// opens one socket (one HttpClient instance) and re-uses it.
        /// https://stackoverflow.com/questions/22560971/what-is-the-overhead-of-creating-a-new-httpclient-per-call-in-a-webapi-client
        /// </summary>
        public static HttpClient HttpClient = new HttpClient();

        #endregion

        /// <summary>
        /// MainWindow initialize
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            try
            {
                ItemsForDropdowns = GetAllItemsForDropdown().OrderBy(i => i.Name);

                BuildItemsDropDownList();

                OsrsItems.DataContext = new ObservableCollection<Item>();

                GetSavedItems();
            }
            catch (Exception e)
            {
                MessageBoxer.Show.Error(e.Message);
            }
        }

        /// <summary>
        /// Get all the saved Items from the json file
        /// </summary>
        private void GetSavedItems()
        {
            if (!File.Exists(SAVED_ITEMS_JSON_FILEPATH))
            {
                ObservableItems = new ObservableCollection<Item>();
            }
            else
            {
                string jsonFileContents = File.ReadAllText(SAVED_ITEMS_JSON_FILEPATH);

                if (String.IsNullOrWhiteSpace(jsonFileContents))
                {
                    throw new FileFormatException("Not a valid json file. Saved file is an invalid .json file, empty, or has whitespace.");
                }

                IEnumerable<Item> items = JsonConvert.DeserializeObject<IEnumerable<Item>>(jsonFileContents);

                ObservableItems = new ObservableCollection<Item>(items);
            }

            ItemsInitialCount = ObservableItems.Count;

            OsrsItems.ItemsSource = ObservableItems;
        }


        /// <summary>
        /// Build the item selection drop down list (combobox)
        /// </summary>
        private void BuildItemsDropDownList()
        {
            comboBox.ItemsSource = ItemsForDropdowns;
        }

        /// <summary>
        /// Get all item names and ids from osrsitems.json
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ItemForDropdown> GetAllItemsForDropdown()
        {
            if (!File.Exists(OSRS_ITEMS_FOR_DROPDOWN))
            {
                throw new FileNotFoundException($"{OSRS_ITEMS_FOR_DROPDOWN} not found! Please redownload OS RS GE Helper or get a new copy of {OSRS_ITEMS_FOR_DROPDOWN} and place it in the same folder as OsRsGeHelper.exe.");
            }

            string jsonFileContents = File.ReadAllText(OSRS_ITEMS_FOR_DROPDOWN);

            return JsonConvert.DeserializeObject<IEnumerable<ItemForDropdown>>(jsonFileContents);
        }


        /// <summary>
        /// Get an item from the OS RS API by itemID
        /// This method converts the item from JSON to an object of type Item
        /// </summary>
        /// <param name="itemID">the item's ID</param>
        /// <returns>The Item</returns>
        private static async Task<Item> GetItemFromOsrsApiAsync(int itemID)
        {
            string url = BASE_OS_RS_API_URL + ITEM_DETAILS_ROUTE + itemID;
            string response = await HttpClient.GetStringAsync(url);

            return JsonConvert.DeserializeObject<ItemWrapper>(response).Item;
        }



        /// <summary>
        /// Add to the ObservableItems collection an item from the OSRS api
        /// </summary>
        /// <param name="itemID">Item's Item.ID</param>
        public static async void AddItemAsync(int itemID)
        {
            ObservableItems.Add(await GetItemFromOsrsApiAsync(itemID));
        }

       




        /// <summary>
        /// Helper to get the item's name from the ObservableItems collection
        /// </summary>
        /// <param name="itemID"></param>
        /// <returns>The item's name. Null if not found.</returns>
        public string GetItemNameByItemID(int itemID)
        {
            return ObservableItems.FirstOrDefault(i => i.ID == itemID)?.Name;
        }

        /// <summary>
        /// Get an item's info from the OS RS API, to add it to the grid
        /// </summary>
        private void btn_addItemAsync_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (comboBox.SelectedValue == null)
                {
                    MessageBoxer.Show.Notification("Choose an item.");
                    return;
                }

                AddItemAsync(Convert.ToInt32(comboBox.SelectedValue));
            }
            catch (Exception ex)
            {
                MessageBoxer.Show.Error(ex.Message);
            }
        }

        /// <summary>
        /// Save the contents
        /// </summary>
        private void btn_save_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }


        /// <summary>
        /// Save the contents of the current grid to a json file
        /// </summary>
        private void Save()
        {
            try
            {
                if (File.Exists(SAVED_ITEMS_JSON_FILEPATH))
                {
                    File.Delete(SAVED_ITEMS_JSON_FILEPATH);
                }
                else
                {
                    File.Create(SAVED_ITEMS_JSON_FILEPATH).Close();
                }

                File.WriteAllText(SAVED_ITEMS_JSON_FILEPATH, JsonConvert.SerializeObject(OsrsItems.Items));

                ItemsInitialCount = OsrsItems.Items.Count;

                MessageBoxer.Show.Notification("Saved!", "Saved");
            }
            catch (Exception e)
            {
                MessageBoxer.Show.Error(e.Message);
            }
        }


        /// <summary>
        /// Clear the datagrid of items and also delete the savedItems.json file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_clear_Click(object sender, RoutedEventArgs e)
        {
            var response =
                MessageBoxer.Show.GetUserYesOrNoOrCancel(
                    "Are you sure you want to delete all contents of the current grid and delete your saved results?",
                    "Are you sure?");

            if (response == System.Windows.Forms.DialogResult.Yes)
            {
                try
                {
                    if (File.Exists(SAVED_ITEMS_JSON_FILEPATH))
                    {
                        File.Delete(SAVED_ITEMS_JSON_FILEPATH);
                    }

                    ObservableItems = new ObservableCollection<Item>();
                    OsrsItems.ItemsSource = ObservableItems;
                }
                catch (Exception ex)
                {
                    MessageBoxer.Show.Error(ex.Message);
                }
            }
        }




        /// <summary>
        /// If the count between the initial items loaded and the items in the collection
        /// arent the same - prompt the user to save.
        /// 
        /// TODO: This could be improved to make sure that only this message is shown
        /// when the user has a different item collection when exiting than loading, but
        /// this is dirtier
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            //If the item count is the same or empty, just return.
            if ((ObservableItems.Count == ItemsInitialCount)
                || (ObservableItems.Count == 0))
            {
                return;
            }

            var response = MessageBoxer.Show.GetUserYesOrNo("Would you like to save before exiting?", "Save?");

            if (response == System.Windows.Forms.DialogResult.Yes)
            {
                Save();
            }
        }


        /// <summary>
        /// Refresh all items in the data grid by hitting the OS RS GE API for new/updated
        /// info about the items
        /// </summary>
        private async void btn_refreshAsync_Click(object sender, RoutedEventArgs e)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            var itemIDs = ObservableItems.Select(i => i.ID);

            var items = new List<Item>();

            //perhaps look into Parralell.ForEach
            foreach (int itemID in itemIDs)
            {
                items.Add(await GetItemFromOsrsApiAsync(itemID));
            }

            ObservableItems = new ObservableCollection<Item>(items);
            OsrsItems.ItemsSource = ObservableItems;
            Mouse.OverrideCursor = Cursors.Arrow;
        }


        /// <summary>
        /// Opens the item in the browser
        /// Trigged by a double click on a row.
        /// TODO: fix when user double clicks or clicks too fast on the remove button
        /// </summary>
        private void OpenItemInBrowser(object sender, MouseButtonEventArgs e)
        {
            // Ensure row was clicked and not empty space
            var row = ItemsControl.ContainerFromElement((DataGrid)sender, (DependencyObject)e.OriginalSource) as DataGridRow;

            var item = row?.Item as Item;

            if (item == null)
            {
                return;
            }

            //replace spaces with underscores so the URL is more readable
            item.Name = item.Name.Replace(' ', '_');

            string item_url = String.Format("http://services.runescape.com/m=itemdb_oldschool/{0}/viewitem?obj={1}", item.Name, item.ID);

            //open the browser (or a new tab) with the item's url
            Process.Start(item_url);
        }

        /// <summary>
        /// Opens the item in the browser, using the os rs wikia
        /// </summary>
        private void btn_openInOsRsWikia(object sender, RoutedEventArgs e)
        {
            string itemName = ((Button)sender).DataContext.ToString().Replace(' ', '_');
            var url = "http://oldschoolrunescape.wikia.com/wiki/" + itemName;

            Process.Start(url);
        }


        /// <summary>
        /// Launches a new form for a user to calculate a certain GP amount for 
        /// the user to sell/buy. The passed in info (from sender) is the Item's ID, 
        /// instead of the Item. This was to correct some sort of Binding FUBAR-ness
        /// that should perhaps be investigated/fixed, when trying to just pass in the Item
        /// as the binding, instead of the Item.ID
        /// </summary>
        private void btn_calculator_Click(object sender, RoutedEventArgs e)
        {
            int itemID = Convert.ToInt32(((Button)sender).DataContext.ToString());

            Item item = ObservableItems.FirstOrDefault(i => i.ID == itemID);

            if (item == null)
            {
                MessageBoxer.Show.Error($"Could not find Item for Item.ID: {itemID}");
                return;
            }

            var calculator = new Calculator
            {
                Item = item,
                lbl_result = {Content = item.CurrentPriceForDisplay}
            };

            calculator.Show();
        }
    }




}
