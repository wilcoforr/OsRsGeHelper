﻿using System;
using Newtonsoft.Json;

namespace OsRsGeHelper
{
    /// <summary>
    /// Wraps the Item class because the API Response is an empty object containing an Item
    /// </summary>
    public class ItemWrapper
    {
        public Item Item { get; set; }
    }

    /// <summary>
    /// These are the items that can be traded, from the OS RS GE API
    /// Has to be refreshed (manually) when new items are added to the game
    /// Probably by incrementally hitting the OS RS GE API and getting items names/IDs
    /// </summary>
    public class ItemForDropdown
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// An item that is sellable on the OS RS GE.
    /// </summary>
    public class Item
    {
        public int ID { get; set; }
        public string Icon { get; set; }
        public string IconLarge { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        [JsonProperty("members")]
        public bool IsMembersItem { get; set; }

        public PriceHistory Current { get; set; }

        public int CurrentPriceAsInt => ConvertOsRsGePriceStringToInt(Current.Price);

        /// <summary>
        /// For a display like: 2650000 (2.65 million) to 2,650,000
        /// ...if ever wanted
        /// </summary>
        public string CurrentPriceForDisplay => CurrentPriceAsInt.ToString("N0");

        //the price histories are pretty much unused
        //user can just double click on a row to get to the official page
        //to look at data
        public PriceHistory Today { get; set; }
        public PriceHistory Day30 { get; set; }
        public PriceHistory Day90 { get; set; }
        public PriceHistory Day180 { get; set; }

        /// <summary>
        /// Since the prices aren't really "real" numbers, like 308.5k for 308,500 gp,
        /// so convert them to a real number (int)
        /// Useful for sorting in the UI and perhaps future uses
        /// </summary>
        /// <param name="osrsgePrice"></param>
        /// <returns></returns>
        public int ConvertOsRsGePriceStringToInt(string osrsgePrice)
        {
            if (osrsgePrice.EndsWith("k"))
            {
                osrsgePrice = osrsgePrice.TrimEnd('k');
                return Convert.ToInt32(Convert.ToDouble(osrsgePrice) * 1000);
            }
            else if (osrsgePrice.EndsWith("m"))
            {
                osrsgePrice = osrsgePrice.TrimEnd('m');
                return Convert.ToInt32(Convert.ToDouble(osrsgePrice) * 1000000);
            }
            else
            {
                return Convert.ToInt32(Convert.ToDouble(osrsgePrice));
            }
        }

        /// <summary>
        /// Converts the real price to the OS RS format
        /// Like from 385,000 gp to 385.00k gp
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public static string ConvertPriceAsIntToOsRsPriceFormat(int price)
        {
            double osrsPrice = price;

            string osrsPriceFormat = osrsPrice.ToString();

            //greater than 10k, less than a million gp
            if (price > 10000 && price < 1000000)
            {
                osrsPriceFormat = (osrsPrice / 1000).ToString("N2") + "k";
            }
            //greater than or equal to a million gp
            else if (price >= 1000000)
            {
                osrsPriceFormat = (osrsPrice / 1000000).ToString("N2") + "m";
            }

            return osrsPriceFormat;
        }

    }

    /// <summary>
    /// Price History is the history of past prices/trends/changes for a specific item
    /// Used in graphs and such in the OS RS GE Services page (web)
    /// </summary>
    public class PriceHistory
    {
        public string Price { get; set; }
        public string Trend { get; set; }
        public string Change { get; set; }
    }
}
