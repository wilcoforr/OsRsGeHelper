﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace OsRsGeHelper
{
    /// <summary>
    /// Interaction logic for Calculator.xaml
    /// </summary>
    public partial class Calculator : Window
    {
        public Item Item { get; set; }

        public Calculator()
        {
            InitializeComponent();
        }

        private void btn_calculatePrice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int itemAmount = Math.Abs(Convert.ToInt32(txtBox_itemAmount.Text));

                int totalPrice = itemAmount * Item.CurrentPriceAsInt;

                lbl_result.Content = Item.ConvertPriceAsIntToOsRsPriceFormat(totalPrice) + " gp";
            }
            catch
            {
                MessageBoxer.Show.Error("Error calculating the total price.\r\nEnter the item amount as a positive integer.");
            }
        }
    }
}
