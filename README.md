![OS RS GE Helper](https://gitlab.com/wilcoforr/OsRsGeHelper/raw/master/logo.png "OS RS GE Helper")


# OS RS GE Helper #

## Old School Runescape Grand Exchange Helper ##

Simple desktop app that you can use to track Runescape items and their prices.

Instead of having tons of bookmarks and tabs open, use this instead!

![Example](https://gitlab.com/wilcoforr/OsRsGeHelper/raw/master/example-home.png)

![Example](https://gitlab.com/wilcoforr/OsRsGeHelper/raw/master/calculator-example.png)